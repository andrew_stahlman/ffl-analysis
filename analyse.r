require(plyr)
require(caret)

rb.cols <- c(
             'integer', # FumL
             'integer', # RushTD
             'integer', # RushYds
             'integer', # GS
             'character', # Name
             'integer', # WeightLbs
             'integer', # Season
             'numeric', # RushAvg
             'integer', # RecTD
             'numeric', # RecAvg
             'character', # DoB
             'integer', # Games
             'integer', # RecYds
             'factor', # Team
             'integer', # Rec
             'character', # RecLng
             'integer', # Fum
             'character', # RushLng
             'integer', # RushAtt
             'integer' # HeightInches
             )

historical.rb <- read.csv("data/rb-since-2000.csv", na.strings=c("--"), colClasses=rb.cols, header=TRUE)
current.rb <- read.csv("data/current-rb.csv", na.strings=c("--"), colClasses=rb.cols, header=TRUE)

rb <- rbind(historical.rb, current.rb)

rb <- rename(rb, replace=c("RushAtt"="Carries"))

rb$TD <- rb$RushTD + rb$RecTD
rb$Yds <- rb$RushYds + rb$RecYds
rb$Touches <- rb$Carries + rb$Rec

rb[is.na(rb$TD),c("TD")] <- 0
rb[is.na(rb$Yds),c("Yds")] <- 0
rb[is.na(rb$Touches),c("Touches")] <- 0
rb[is.na(rb$FumL),c("FumL")] <- 0

rb[which(rb$Name == "Ricky Williams" & rb$Team == "Indianapolis Colts"),]$Name = "Ricky Williams (Colts)"

rb_points <- c("Yds"=.1, "TD"=6, "FumL"=-2)

add.exp <- function(d) {
    calcExp <- function(p) {
        p$Exp <- p$Season - min(p$Season)
        return (p)
    }
    do.call(rbind, by(data=d, d$Name, FUN=calcExp))
}
combine.rows <- function(rows) {
    cols.to.add <- c("Touches", "TD", "Yds", "Games", "TotalPoints")
    row <- rows[1,]
    row[cols.to.add] <- sapply(cols.to.add, function(c) { row[c] <- sum(rows[c]) })
    return (row)
}

fix.traded <- function(d) {
    ddply(d, .(Name, Season), function(d) combine.rows(d))
}

add.points <- function(d, mult) {
    for (stat in names(mult)) {
        multiplier <- mult[stat]
        colLabel <- paste(stat, "Points", sep="")
        d[colLabel] <- d[,stat] * multiplier
    }

    d$TotalPoints = d$YdsPoints + d$TDPoints + d$FumLPoints

    return(d)
}

rb <- add.points(rb, rb_points)

birthYear <- function(dob) {
    as.numeric(substr(dob, 1, 4))
}

calcAge <- function(d) {
    d$Age <- as.numeric(d$Season) - birthYear(d$DoB)
    return(d)
}

rb <- calcAge(rb)

addPrevSeason <- function(d, rec) {
    prev <- d[which(
            d$Name == rec$Name &
            d$Season == rec$Season - 1),]
    if (empty(prev) == FALSE) {
        # 1 player could have multiple entries for a season in case of trade
        return (c(
                  "PrevPoints"=sum(prev$TotalPoints),
                  "PrevTouches"=sum(prev$Touches),
                  "PrevGamesMissed"= 16-sum(prev$Games),
                  "PrevSeason"=unique(prev$Season)))
    } else {
        return (c(
                  "PrevPoints"=NA,
                  "PrevTouches"=NA,
                  "PrevGamesMissed"=NA,
                  "PrevSeason"=NA))
    }
}

addPrevCols <- function(d) {
    r = matrix(nrow=0, ncol=4)
    for (i in 1:dim(d)[1]) {
        r <- rbind(r,addPrevSeason(d, d[i,]))
    }
    return(cbind(d,r))
}

rb <- addPrevCols(rb)
rb <- rb[,c(
            "Name",
            "Season",
            "Team",
            "Age",
            "Touches",
            "TD",
            "Yds",
            "Games",
            "PrevSeason",
            "PrevTouches",
            "PrevPoints",
            "PrevGamesMissed",
            "TotalPoints")]


rb$MissedTime <- rb$PrevGamesMissed >= 4
rb <- fix.traded(rb)

proj <- read.csv("data/fantasy-pros-rb-proj.tsv", sep="\t")
proj <- rename(proj, replace=c("FantasyPoints"="Proj",
                               "Player"="Name"))
proj <- proj[,c("Name","Proj","Season")]

rb <- merge(rb, proj, by=c("Name", "Season"))
rb <- subset(rb, is.na(Proj) == FALSE & is.na(PrevSeason) == FALSE)
rb$Importance <- rb$TotalPoints + rb$Proj

rb$ProjErr <- rb$TotalPoints - rb$Proj
rb$ProjRMSE <- sqrt(mean(rb$ProjErr^2, na.rm=TRUE))

graph <- function (d) {
    ggplot(d, aes(y=TotalPoints)) + geom_point(aes(x=Proj, colour="Proj")) + geom_point(aes(x=ModelPred, colour="Model")) + geom_text(data=d, aes(y=TotalPoints, x=Proj, label=paste(Name,"-",Season), size=1)) + geom_text(data=d, aes(y=TotalPoints, x=ModelPred, label=paste(Name,"-",Season), size=1)) + geom_abline()
}

rmse <- function(err) {
    sqrt(mean(err^2, na.rm=TRUE))
}

# Partition test and train set
train <- createDataPartition(rb$TotalPoints, times=1, p=.7, list=FALSE)
rb.train <- rb[train,]
rb.test <- rb[-train,]

top.x <- function(d, x=10) {
    head(d[with(d, order(-Importance)),], n=x)
}
bottom.x <- function(d, x=10) {
    tail(d[with(d, order(-Importance)),], n=x)
}

fit.1 <- lm(TotalPoints ~ PrevTouches + PrevPoints + MissedTime*Age + Proj, data=rb.train)
fit.2 <- lm(TotalPoints ~ PrevTouches + PrevPoints + MissedTime*Age + Proj, data=rb.train, weights=sapply(rb.train$Importance, FUN=function(x) max(0, x)))

make.pred <- function (fit, d=rb.test) {
    pred.with.proj <- predict(fit, newdata=d)
    d$ModelPred <- pred.with.proj
    d$ModelErr <- d$TotalPoints - d$ModelPred
    d$ModelRMSE <- sqrt(mean(d$ModelErr^2))
    return (d)
}

