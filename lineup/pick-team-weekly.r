library(plyr)

proj.cols <- c("name", "pos", "proj")

x <- read.csv("data/fantasy-pros/wk11/week11-proj.tsv", sep="\t", header = TRUE, col.names = proj.cols, strip.white = TRUE)

sal.cols <- c("name", "pos", "cost")
y <- read.csv("data/fantasy-pros/wk11/fd-salary-late.tsv", sep="\t", header=TRUE, col.names = sal.cols, strip.white=TRUE)
x <- merge(x, y, on="name")

pos.i <- model.matrix( ~ pos - 1, data=x)
x <- cbind(x, pos.i)

filter.quantile <- function(d, x) {
    ddply(d, c("pos", "week"), function(p) {
          cutoff <- quantile(p$proj, c(x))[[1]]
          return(p[which(p$proj >= cutoff),])
    })
}

add.noise <- function(x) {
    x <- x[which(x$pos != "D"),]
    old <- read.csv("2013-actuals.tsv", sep="\t", header=TRUE, strip.white=TRUE)
    old <- old[which(old$pos != "D"),]
    old$pos <- factor(old$pos)
    old <- old[which(!(old$name == "Alex Smith" & old$pos == "TE")),]
    old.proj <- read.csv("ff-today-proj-2013.tsv", sep="\t", header=TRUE, strip.white=TRUE)
    old <- merge(old, old.proj, on="name")
    old <- ddply(old, "pos", function(p) { filter.quantile(p, .2) }) # top 80% of projected points at each position
    old$res <- old$pts - old$proj
    by.pos <- ddply(old, "pos", summarise, 
                    res.mean=mean(res), 
                    res.sd=sd(res))

    noise <- function (d, mu, sigma) { 
        rnorm(length(d), mean=mu, sd=sigma) 
    } 

    ddply(x, "pos", function (all.p) {
          this.pos <- all.p[1,"pos"]
          if (this.pos != "D") {
              mu <- by.pos[which(by.pos$pos==this.pos), "res.mean"]
              sigma <- by.pos[which(by.pos$pos==this.pos), "res.sd"]
          } else {
              mu <- 0
              sigma <- 2
          }
          all.p$randomized <- all.p$proj + noise(all.p$proj, mu, sigma) * .3 # arbitary scale down
          all.p$proj <- all.p$randomized
          return(all.p)
    });
}

library("lpSolveAPI")
library("lpSolve")

cols <- c("name", "proj", "cost", "posQB", "posRB", "posTE", "posWR", "posK")
pick <- function(x) {
    mat <- x[,cols]

    budget <- 60000 - 4800
    f.obj <- mat$proj
    f.cons <- rbind(mat$cost, mat$posQB, mat$posRB, mat$posWR, mat$posTE, mat$posK)
    f.dir <- c("<=", "=", "=", "=", "=", "=")
    f.rhs <- c(budget, 1, 2, 3, 1, 1)

    sol <- lp("max", f.obj, f.cons, f.dir, f.rhs, all.bin=TRUE)
    score <- sol$objval
    team <- mat[which(sol$solution == 1),]
}

