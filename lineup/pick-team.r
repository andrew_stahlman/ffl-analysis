library(plyr)
library("lpSolveAPI")
library("lpSolve")

#source('bayesff/bff-data.r')
#source('data/ff-today/load-data.r', chdir=TRUE)

# target: > 118.88 (median)
cols <- c("name", "proj", "pts", "cost", "posQB", "posRB", "posTE", "posWR", "posK")
make.proj <- function(x, opt.col="proj") {
    
    x <- x[which(x$proj >= 0 & x$cost >= 0 & x$pts >= 0),]

    defense.score <- 6
    budget <- 60000 - 5200 # average defense costs 5200, scores 6 points

    f.obj <- x[,opt.col]
    f.cons <- rbind(x$cost, x$posQB, x$posRB, x$posWR, x$posTE, x$posK)
    f.dir <- c("<=", "=", "=", "=", "=", "=")
    f.rhs <- c(budget, 1, 2, 3, 1, 1)

    sol <- lp("max", f.obj, f.cons, f.dir, f.rhs, all.bin=TRUE)
    team <- x[which(sol$solution == 1),]
    proj.score <- sum(x[which(sol$solution == 1), "proj"]) + defense.score
    actual.score <- sum(x[which(sol$solution == 1), "pts"]) + defense.score
    return(new.team(team, proj.score, actual.score))
}

new.team <- function (team, proj.pts, actual.pts) {
    x <- list("team"=team, "proj.pts"=proj.pts, "actual.pts"=actual.pts)
    class(x) <- "team"
    return(x)
}

print.team <- function(x) {
    cat("Projected: ", x$proj.pts, "\nActual: ", x$actual.pts, "\n\n")
    print(x$team)
}
