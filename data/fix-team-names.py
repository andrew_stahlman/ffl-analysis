import csv, sys

d = {}
with open('team-names.csv', mode='r') as infile:
    reader = csv.reader(infile)
    d = dict((rows[1],rows[0]) for rows in reader)

print d

with open(sys.argv[1] + '-fixed.csv', mode='w') as outfile:
    csvFile = csv.writer(outfile, delimiter=',', quotechar='"')
    with open(sys.argv[1], mode='r') as infile:
        reader = csv.reader(infile)
        for row in reader:
            if (row[0] in d):
                row[0] = d[row[0]]
            csvFile.writerow(row)

