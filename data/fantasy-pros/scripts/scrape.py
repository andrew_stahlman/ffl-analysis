import urllib2, re, csv, time
import time
from bs4 import BeautifulSoup

def main():
    gatherAllData()

def loadSoup(pos="rb"):
    url = "http://www.fantasypros.com/nfl/projections/%s.php" % (pos.lower())
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    return soup

def extractFromPage(pos):
    soup = loadSoup(pos)
    rows = [row for row in soup.find('table', {"id" : 'data'}).find_all('tr')][2:]
    entries = []
    for row in rows:
        cells = row.find_all('td')
        entries.append([parseCellContents(cells[0].find('a').text), pos.upper(), parseCellContents(cells[-1].text)])
    return entries 
 
def gatherAllData():

    pos = ["RB", "QB", "WR", "TE", "K"]
    headers = ["name", "pos", "proj"]
    filename = "export-%s.tsv" % (time.strftime("%Y-%m-%d %H:%M"))
    csvFile = csv.writer(open(filename, 'w'), delimiter='\t', quotechar='"')
    csvFile.writerow(headers)
    for p in pos:
        print "Reading page " + p
        data = extractFromPage(p)
        if not data:
            break
        for record in data:
            csvFile.writerow(record)

def parseCellContents(contents):
    s = contents.strip().replace('$', '') # parse money as numeric
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

if __name__ == "__main__": main()
