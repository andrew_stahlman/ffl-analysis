library(plyr)

load.data <- function(week=NA) {
    care.cols <- c("name", "pos", "pts", "week")
    actuals <- read.csv("2013-actuals.tsv", sep="\t", header=TRUE, col.names = care.cols)
    care.cols <- c("name", "pos", "proj", "week")
    projections <- read.csv("ff-today-proj-2013.tsv", sep="\t", header=TRUE, col.names = care.cols)

    salary <- read.csv("fd-salary-2013.csv", sep=",", header=TRUE)
    salary <- salary[-which(salary$name == "Alex Smith" & salary$team == "cin"),] # Sorry Alex Smith, CIN TE...
    #te[which(te$name == "Alex Smith"),"pos"] <- "QB"
    salary[which(salary$name == "Alex Smith"), "pos"] <- "QB"
    salary <- salary[,c("name", "week", "cost")]

    x <- merge(actuals, salary, on=c("name", "week"), all.x=TRUE)
    x <- merge(x, projections, on=c("name", "week"), all.x=TRUE)
    x <- x[which((x$proj > 0 | x$pts > 0) & x$cost > 0),]

    pos.i <- model.matrix( ~ pos - 1, data=x)
    x <- cbind(x, pos.i)
    if (!is.na(week)) {
        x <- x[which(x$week == week),]
    }
    return(x)
}

