import urllib2, re, csv, time
import time
from bs4 import BeautifulSoup

positions = { 
        10: "QB",
        20 : "RB",
        30 : "WR",
        40 : "TE",
        99 : "D",
        80 : "K" 
      }

def main():
    gatherAllData()

def loadSoup(week=1, pos=99):
    url = "http://fftoday.com/stats/playerstats.php?Season=2013&GameWeek=%d&PosID=%d&LeagueID=1" % (week, pos)
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    return soup

def extractFromPage(week, pos):
    soup = loadSoup(week, pos)
    rows = soup.find_all('table')[3].find_all('tr')[16:]
    entries = []
    for row in rows:
        cells = row.find_all('td')
        print cells
        entries.append([parseCellContents(cells[0].find('a').text), positions[pos], parseCellContents(cells[-1].text)])
    return entries 
 
def gatherAllData():

    headers = ["name", "pos", "pts", "week"]
    filename = "actuals-export-%s.csv" % (time.strftime("%Y-%m-%d %H:%M"))
    csvFile = csv.writer(open(filename, 'w'), delimiter=',', quotechar='"')
    csvFile.writerow(headers)
    for week in range(1,18):
        for p in positions.keys():
            print "Reading page " + str(p) + " for week " + str(week)
            data = extractFromPage(week, p)
            if not data:
                break
            for record in data:
                record.append(week)
                csvFile.writerow(record)

def parseCellContents(contents):
    print contents
    s = contents.strip().replace('$', '') # parse money as numeric
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

if __name__ == "__main__": main()
