import urllib2, re, csv, time
from bs4 import BeautifulSoup

yearRe = re.compile('[0-9]{4}')
dateRe = re.compile("(\d{1,2})\/(\d{1,2})\/(\d{4})")
baseURL = "http://www.nfl.com"

def extractFromPage(pageIndex):
    print "Fetching data from page " + str(pageIndex)
    url = "http://www.nfl.com/players/search?category=position&playerType=current&conference=ALL&d-447263-p=%d&filter=runningback" % (pageIndex)
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    players = []
    for row in soup.select('table#result > tbody > tr'):
        player = {}
        cells = row.find_all('td')
        player['url'] = cells[0].a['href']
        player['name'] = cells[0].a.string
        years = map(int, yearRe.findall(cells[2].string))
        player['start_year'] = years[0]
        player['end_year'] = years[1]
        players.append(player)

    return filter(lambda p: p['end_year'] is None or p['end_year'] >= 2000, players)

def parseCellContents(contents):
    s = contents.strip()
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

def parseSeason(seasonRow):
    cols = ["Season", "Team", "Games", "GS", "RushAtt", "RushYds", "RushAvg", "RushLng", "RushTD", "Rec", "RecYds", "RecAvg", "RecLng", "RecTD", "Fum", "FumL"]
    allCells = seasonRow.select('td')
    plainTextCells = filter(lambda x: x.string is not None, allCells)
    allButTeam = [parseCellContents(cell.string) for cell in plainTextCells]
    team = allCells[1].find('a').text
    allFields = [allButTeam[0], team] + allButTeam[1:]
    allFields = [None if field == "--" else field for field in allFields]
    return dict(zip(cols, allFields))
    

def parsePlayerBio(soup):
    cols = ['Name', 'HeightInches', 'WeightLbs', 'DoB']
    vals = [list(soup.find(id="player-bio").stripped_strings)[i] for i in [0,2,4,8]]
    vals = [parseCellContents(x.replace(":","")) for x in vals]
    dateFields = dateRe.search(vals[3])
    if dateFields:
        vals[3] = "%s-%s-%s" % tuple([dateFields.group(i) for i in reversed(range(1,4))])
    else:
        vals[3] = "--"
    heightFields = re.search('(\d)-(\d+)', vals[1])
    feet = int(heightFields.group(1))
    inches = int(heightFields.group(2))
    vals[1] = (feet * 12) + inches
    if not (isinstance(vals[2], int)):
        vals[2] = int(re.search("\d+", vals[2]).group(0))
    d = dict(zip(cols,vals))
    return d

def parsePlayerPage(d):
    try:
        soup = BeautifulSoup(urllib2.urlopen(baseURL + d['url']).read())
    except urllib2.HTTPError:
        try:
            soup = BeautifulSoup(urllib2.urlopen(baseURL + d['url']).read())
        except urllib2.HTTPError:
            print "Error fetching " + baseURL + d['url']
            return {}
    # 3 table headers and the last row is just a summation of all seasons
    # every other row is blank
    allSeasons = soup.select('#player-stats-wrapper tr')[3:-1][::2]
    parsedSeasons = [parseSeason(season) for season in allSeasons]
    playerData = parsePlayerBio(soup)
    playerData['allSeasons'] = parsedSeasons
    return playerData

def gatherPageData(i):
    players = extractFromPage(i)
    return [parsePlayerPage(p) for p in players]

def toCSVRows(rec):
    seasons = rec.pop("allSeasons")
    rows = []
    for s in seasons:
        row = rec.copy()
        row = dict((row.items() + s.items()))
        rows.append(row)
    return rows

def gatherAllData(start=1):

    if (start == 1):
        csvFile = csv.writer(open('players.csv', 'w'), delimiter=',', quotechar='"')
    else:
        csvFile = csv.writer(open('players.csv', 'a'), delimiter=',', quotechar='"')
    d = []
    i = start
    wroteHeaders = False
    while True:
        print "Reading page " + str(i)
        data = gatherPageData(i)
        i += 1
        if not data:
            break
        for record in data:
            rows = toCSVRows(record)
            if not wroteHeaders:
                csvFile.writerow(rows[0].keys())
                wroteHeaders = True
            for row in rows:
                csvFile.writerow(row.values())
