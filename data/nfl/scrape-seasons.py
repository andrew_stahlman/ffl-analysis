import urllib2, re, csv
from bs4 import BeautifulSoup

yearRe = re.compile('[0-9]{4}')
dateRe = re.compile("(\d{1,2})\/(\d{1,2})\/(\d{4})")
baseURL = "http://www.nfl.com"

def extractFromPage(pageIndex):
    print "Fetching data from page " + str(pageIndex)
    url = "http://www.nfl.com/players/search?category=position&playerType=historical&d-447263-p=%d&conference=ALL&filter=runningback" % (pageIndex)
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    players = []
    for row in soup.select('table#result > tbody > tr'):
        player = {}
        cells = row.find_all('td')
        years = map(int, yearRe.findall(cells[2].string))
        url = cells[0].a['href']
        url = url.replace("profile","gamelogs")
        yearRange = range(years[0], years[1] + 1)
        player['urls'] = dict(zip(yearRange, ["%s?season=%d" % (url, s) for s in yearRange]))
        player['name'] = cells[0].a.string
        player['start_year'] = years[0]
        player['end_year'] = years[1]
        players.append(player)

    return filter(lambda p: p['end_year'] is None or p['end_year'] >= 2000, players)

def parseCellContents(contents):
    s = contents.strip().replace("\t", "").replace("\n", "").replace("\r", "")
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s

def parsePlayerBio(soup):
    cols = ['Name', 'HeightInches', 'WeightLbs', 'DoB']
    vals = [list(soup.find(id="player-bio").stripped_strings)[i] for i in [0,2,4,8]]
    vals = [parseCellContents(x.replace(":","")) for x in vals]
    dateFields = dateRe.search(vals[3])
    if dateFields:
        vals[3] = "%s-%s-%s" % tuple([dateFields.group(i) for i in reversed(range(1,4))])
    else:
        vals[3] = "--"
    heightFields = re.search('(\d)-(\d+)', vals[1])
    feet = int(heightFields.group(1))
    inches = int(heightFields.group(2))
    vals[1] = (feet * 12) + inches
    if not (isinstance(vals[2], int)):
        vals[2] = int(re.search("\d+", vals[2]).group(0))
    d = dict(zip(cols,vals))
    return d

def parseSeason(player, year):
    print player
    url = player['urls'][year]
    soup = BeautifulSoup(urllib2.urlopen(baseURL + url))
    tables = soup.select('#player-stats-wrapper table')
    try:
        table = [t for t in tables if t.findAll(text="Regular Season")][0]
    except IndexError:
        return None
    rows = table.findAll('tr')
    headers = [cell.string for cell in rows[1].findAll('td')]
    headers.insert(3, "WinOrLoss")
    dups = ["Yds", "Avg", "Lng", "TD"]
    for dup in dups:
        headers[headers.index(dup)] = "Rush" + dup
        headers[headers.index(dup)] = "Rec" + dup
    rows = rows[2:-1:2]
    weeks = [[parseCellContents(cell) for cell in list(row.stripped_strings)] for row in rows]
    season = [dict(zip(headers, week)) for week in weeks]
    for game in season:
        game['name'] = player['name']
        game['season'] = year
    return season

def parsePlayer(player):
    allSeasons = [parseSeason(player, season) for season in player['urls'].keys()]
    allSeasons = filter(lambda x: x is not None, allSeasons)
    # flatten list
    return [game for season in allSeasons for game in season]

def gatherAllData(pageStart = 1):
    d = []
    i = pageStart
    while i < 3:
        print "Reading page " + str(i)
        page = extractFromPage(i)
        #recs = [parsePlayer(player) for player in page]
        i += 1
        if not page:
            break
        for player in page:
            d.extend(parsePlayer(player))
    return d

