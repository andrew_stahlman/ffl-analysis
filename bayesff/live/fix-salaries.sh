#!/bin/sh

awk -F"\t" '{print $2}' $1 | perl -lne 'if (m/(.+)[POQD]$/) { print $1 } else { print $_ }' > /tmp/fixed-names.txt
cut -f 1,3-6 $1 > /tmp/without-names.txt
paste /tmp/fixed-names.txt /tmp/without-names.txt
