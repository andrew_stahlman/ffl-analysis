import urllib2, re, csv, time
from bs4 import BeautifulSoup

def loadSoup(week):
    print "Fetching data for week " + str(week)
    url = "http://web.archive.org/web/20130907162038/http://fantasy.nfl.com/research/projections?position=8&statCategory=projectedStats&statSeason=2013&statType=weekProjectedStats&statWeek=%d" % (week)
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    return soup

def extractFromPage(week):
    soup = loadSoup(week)
    teams = soup.find_all('table')[3].find_all('a', 'playerNameFull')
    proj = soup.find_all('table')[3].find_all('a', 'playerWeekProjectedPts')
    
    rows = table.find_all('tr')
    care = [r for r in rows if "$" in r.text]
    entries = []
    for entry in care:
        x = [parseCellContents(field.text) for field in entry]
        name = x[0].split(',')
        if len(name) > 1:
            name.reverse()
            x[0] = ' '.join(name).strip()
        entries.append(x)
    return entries
 
def gatherAllData(start=1, end=17):

    if (start == 1):
        csvFile = csv.writer(open('salary-data.csv', 'w'), delimiter=',', quotechar='"')
        csvFile.writerow(["name", "team", "opp", "pts", "cost", "week"])
    else:
        csvFile = csv.writer(open('salary-data.csv', 'a'), delimiter=',', quotechar='"')
    i = start
    while i <= end:
        print "Reading page " + str(i)
        data = extractFromPage(i)
        if not data:
            break
        for record in data:
            record.append(i)
            csvFile.writerow(record)
        i += 1

def parseCellContents(contents):
    s = contents.strip().replace('$', '') # parse money as numeric
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


