library(plyr)


load.data <- function(week=NA) {
    care.cols <- c("season", "week", "pos", "name", "beta", "proj", "pts", "llh", "P05", "P25", "P50", "P75", "P95", "hilow")
    rb <- read.csv("rb-data-2013.tsv", sep="\t", header=TRUE, col.names = care.cols)
    qb <- read.csv("qb-data-2013.tsv", sep="\t", header=TRUE, col.names = care.cols)
    wr <- read.csv("wr-data-2013.tsv", sep="\t", header=TRUE, col.names = care.cols)
    te <- read.csv("te-data-2013.tsv", sep="\t", header=TRUE, col.names = care.cols)
    names(te)[names(te) == "actual"] = "pts" # column headers aren't consistent...

    salary <- read.csv("fd-salary-2013.csv", sep=",", header=TRUE)
    salary <- salary[-which(salary$name == "Alex Smith" & salary$team == "cin"),] # Sorry Alex Smith, CIN TE...
    te[which(te$name == "Alex Smith"),"pos"] <- "QB"
    salary[which(salary$name == "Alex Smith"), "pos"] <- "QB"
    salary <- salary[,c("name", "week", "cost")]

    df <- ldply(list(qb, rb, wr, te), rbind)
    x <- merge(df, salary, on=c("player", "week"), all.x=TRUE)
    x <- x[which((x$proj > 0 | x$pts > 0) & x$cost > 0),]

    pos.i <- model.matrix( ~ pos - 1, data=x)
    x <- cbind(x, pos.i)
    if (!is.na(week)) {
        x <- x[which(x$week == week),]
    }
    return(x)
}

