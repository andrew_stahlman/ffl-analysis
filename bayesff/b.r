library(plyr)

cols <- c("name", "pos", "ppg", "num.games", "matchup", "cost")

x <- read.csv("live/late-games-cleaned.tsv", sep="\t", header =TRUE, col.names = cols, strip.white = TRUE)
y <- read.csv("live/week7-all-proj.tsv", sep="\t", header=TRUE, strip.white=TRUE)
x <- merge(x, y, on="name")

pos.i <- model.matrix( ~ pos - 1, data=x)
x <- cbind(x, pos.i)

#cols <- c("name", "proj", "cost", "posQB", "posRB", "posTE", "posWR", "posD", "posK")
cols <- c("name", "proj", "cost", "posQB", "posRB", "posTE", "posWR")
mat <- x[,cols]

library("lpSolveAPI")
library("lpSolve")

budget <- 60000 - (9000) # no D/ST projections, so just assume an average cost of 5200
f.obj <- mat$proj
#f.cons <- rbind(mat$cost, mat$posQB, mat$posRB, mat$posWR, mat$posTE, mat$posD, mat$posK)
f.cons <- rbind(mat$cost, mat$posQB, mat$posRB, mat$posWR, mat$posTE)
#f.dir <- c("<=", "=", "=", "=", "=", "=", "=")
f.dir <- c("<=", "=", "=", "=", "=")
#f.rhs <- c(budget, 1, 2, 2, 1, 1, 1)
f.rhs <- c(budget, 1, 2, 3, 1)

sol <- lp("max", f.obj, f.cons, f.dir, f.rhs, all.bin=TRUE)
score <- sol$objval
team <- mat[which(sol$solution == 1),]
