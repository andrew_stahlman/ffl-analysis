library(plyr)

#source('bff-data.r')
source('data/ff-today/load-data.r', chdir=TRUE)

# target: > 118.88 (median)
cols <- c("name", "proj", "pts", "cost", "posQB", "posRB", "posTE", "posWR")
make.proj <- function(x, opt.col="proj") {

    library("lpSolveAPI")
    library("lpSolve")

    budget <- 60000 - 9900 # no K + D/ST projections, so just assume average value

    f.obj <- x[,opt.col]
    f.cons <- rbind(x$cost, x$posQB, x$posRB, x$posWR, x$posTE)
    f.dir <- c("<=", "=", "=", "=", "=")
    f.rhs <- c(budget, 1, 2, 3, 1)

    sol <- lp("max", f.obj, f.cons, f.dir, f.rhs, all.bin=TRUE)
    team <- x[which(sol$solution == 1),]
    proj.score <- sum(x[which(sol$solution == 1), "proj"])
    actual.score <- sum(x[which(sol$solution == 1), "pts"])
    return(new.team(team, proj.score, actual.score))
}

new.team <- function (team, proj.pts, actual.pts) {
    x <- list("team"=team, "proj.pts"=proj.pts, "actual.pts"=actual.pts)
    class(x) <- "team"
    return(x)
}

print.team <- function(x) {
    cat("Projected: ", x$proj.pts, "\nActual: ", x$actual.pts, "\n\n")
    print(x$team)
}
