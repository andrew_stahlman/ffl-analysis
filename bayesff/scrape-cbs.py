import urllib2, re, csv, time
from bs4 import BeautifulSoup

def loadSoup(week):
    print "Fetching data for week " + str(week)
    url = "http://fantasynews.cbssports.com/fantasyfootball/stats/weeklyprojections/K/%d/avg/standard" % (week)
    resultsHTML = urllib2.urlopen(url)
    soup = BeautifulSoup(resultsHTML)
    return soup

def extractFromPage(week):
    soup = loadSoup(week)
    rows = [row for row in s.find('table', 'data').find_all('tr')][2:]
    print rows
    entries = []
    for row in rows:
        cells = row.find_all('td')
        print cells
        entries.append([parseCellContents(cells[0].find('a').text), parseCellContents(cells[-1].text)])
    return entries 
 
def gatherAllData(start=1, end=17):

    if (start == 1):
        csvFile = csv.writer(open('cbs-data.csv', 'w'), delimiter=',', quotechar='"')
        csvFile.writerow(["name", "proj", "week"])
    else:
        csvFile = csv.writer(open('cbs-data.csv', 'a'), delimiter=',', quotechar='"')
    i = start
    while i <= end:
        print "Reading page " + str(i)
        data = extractFromPage(i)
        if not data:
            break
        for record in data:
            record.append(i)
            csvFile.writerow(record)
        i += 1

def parseCellContents(contents):
    print contents
    s = contents.strip().replace('$', '') # parse money as numeric
    try:
        return int(s.replace(',',''))
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


