require(plyr)

cols.care <- c("Name", "Team", "G", "Yds", "TD", "Yds.1", "TD.1", "FumL")
rb <- read.csv('rb.csv')[cols.care]
rb <- rename(rb, replace=c("TD"="RushTD","Yds"="RushYds", "Yds.1" = "RecYds", "TD.1" = "RecTD"))
ages <- read.csv('rb-age-fixed.csv')

adp <- read.csv('adp-2.csv')[c("Name", "ADP", "Overall")]
adp <- rename(adp, replace=c("ADP"="ADPRound", "Overall"="ADP"))
rb <- merge(x=rb, y=adp, by="Name")
rb <- merge(x=rb, y=ages, by="Name", all.x=TRUE)

rb$TD <- rb$RushTD + rb$RecTD
rb$Yds <- rb$RushYds + rb$RecYds

rb_points <- c("Yds"=.1, "TD"=6, "FumL"=-2)

addPoints <- function(d, mult) {
    for (stat in names(mult)) {
        multiplier <- mult[stat]
        colLabel <- paste(stat, "Points", sep="")
        d[colLabel] <- d[,stat] * multiplier
    }

    d$TotalPoints = d$YdsPoints + d$TDPoints + d$FumLPoints

    return(d)
}

rb <- addPoints(rb, rb_points)
rb <- rb[which(rb$G >= 8),]
rb$PPG <- rb$TotalPoints / rb$G

plotRB_PPG_vs_ADP <- function (d) {
    round = d$ADP / 8
    plot(round, d$PPG,
         main = "RB Draft Round (8 team) vs PPG",
         xlab = "Round",
         ylab = "PPG",
         col = "blue",
         lty = "solid",
         cex = 1)
    text(round, d$PPG, labels = d$Name, cex=.7, pos=3)
}

plotAge_vs_Points <- function (d) {
    plot(d$Age, d$PPG,
         main = "RB Age vs Points",
         xlab = "Age",
         ylab = "PPG",
         col = "blue",
         lty = "solid",
         cex = 1)
    text(d$Age, d$PPG, labels = d$Name, cex=.7, pos=3)
}
