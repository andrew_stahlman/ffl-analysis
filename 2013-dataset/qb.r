require(plyr)
cols.care <- c("Name", "Team", "G", "Yds", "TD", "Int", "Rush", "Yds.1", "TD.1", "FumL")
qb <- read.csv('qb.csv')[cols.care]
qb <- rename(qb, replace=c("Yds.1" = "RushYds", "TD.1" = "RushTD"))

adp <- read.csv('adp-2.csv')[c("Name", "ADP", "Overall")]
adp <- rename(adp, replace=c("ADP"="ADPRound", "Overall"="ADP"))
#adp <- rename(adp, replace=c("Player" = "Name"))[c("Name", "ADP")]

#qb <- merge(x=qb, y=adp, by="Name", all.x=TRUE)
qb <- merge(x=qb, y=adp, by="Name")
qb$Turnover <- qb$Int + qb$FumL

qb_points  <-c("Yds"=.04, "TD"=4, "Turnover"=-2, "RushYds"=.1, "RushTD"=6)

addPoints <- function(d, mult) {
    for (stat in names(mult)) {
        multiplier <- mult[stat]
        colLabel <- paste(stat, "Points", sep="")
        d[colLabel] <- d[,stat] * multiplier
    }

    d$TotalPoints = d$YdsPoints + d$TDPoints + d$TurnoverPoints + d$RushYdsPoints + d$RushTDPoints

    return(d)
}

qb <- addPoints(qb, qb_points)
qb <- qb[which(qb$G >= 8),]
qb$PPG <- qb$TotalPoints / qb$G

plotQB_PPG_vs_ADP <- function (d) {
    round = d$ADP / 8
    plot(round, d$PPG,
         main = "QB Draft Round (8 team) vs PPG",
         xlab = "Round",
         ylab = "PPG",
         col = "blue",
         lty = "solid",
         cex = 1)
    text(round, d$PPG, labels = d$Name, cex=.7, pos=3)
}


